//inicializamos el array pokemon para llamarlo desde las diferentes partes del código
let pokemons = [];
let currentNode = document.querySelector('div1');

//cuando abrimos la ventana forzamos a que abra de inmediato lo que contenga esta función
window.onload = function(){
    init();
    
    console.log('hola')
}

const init = async ()=>{
    try {
        containerElement();
        //le damos valor al array de pokemons con la función que lee sobre la api y lo retorna en una lista de array
        pokemons = await getAllPokemons();
        printAllPokemons();
    } catch (error) {
        console.log(error);
    }
}

const getAllPokemons = async () =>{

    const resultAPI = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=150&offset=0`); //realizamos la llamada a la API instanciandolo en una variable
    const resultJSON = await resultAPI.json(); //le damos formato JSON ***importante meterle el await ya que necesitamos que realice esta lectura sin pasarse a otra línea
    //console.log(resultJSON);
    //retornamos el mapeo que vamos a realizar a la variable json para instanciarlo en la variable que vamos a manejar en toda la app
    const listPokemons = resultJSON.results.map((pokemon,index)=>{ //si en el map de segundo parametro de entrada ponemos index, este retomara y nos devolvera el indice por el que se encuentra
        return {
            namePokemon: pokemon.name,
            keyPokemon: index+1, 
            img:`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index + 1}.png`
        }
    })
    //una vez que ya tenemos la lectura y mapeo realizado lo devolvemos con el return
    //console.log(listPokemons)
    return listPokemons;
}

//pintamos sobre la lectura que realizamos en el HTML
const printAllPokemons = () =>{
    const $$lu = document.querySelector('.container');
    pokemons.map((pokemon)=>{
        //creo los elementos
        console.log(pokemon);
        const $$li = document.createElement('li');
        const $$div = document.createElement('div');
        const $$title = document.createElement('h3');
        const $$p = document.createElement('p');
        const $$img = document.createElement('img');
        //clases
        $$div.className= 'card'
        $$title.className= 'card__title';
        $$p.className = 'card__id';
        $$img.className = 'card__img';
        //identificadores
        $$div.id = pokemon.keyPokemon;

        //asignamos valores
        $$title.innerHTML = pokemon.namePokemon;
        $$p.innerHTML = 'Pokemon - ' + pokemon.keyPokemon;
        $$img.src = pokemon.img;

        //juntamos los elementos
        $$div.appendChild($$title);
        $$div.appendChild($$img);
        $$div.appendChild($$p);
        
        $$li.appendChild($$div);

        $$lu.appendChild($$li);

    })
}



//Métodos construcción cuerpo html

const containerElement = () =>{
    const elementContainer = document.createElement('ul');
    elementContainer.className = 'container';
    document.body.insertBefore(elementContainer,currentNode);
}